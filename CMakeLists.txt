# Copyright (C) 2021 Huawei Inc.
# SPDX-FileCopyrightText: Huawei Inc.
# SPDX-License-Identifier: Apache-2.0
# Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
# Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>

cmake_minimum_required(VERSION 3.16.5)
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(doorlock LANGUAGES CXX)
target_include_directories(app PRIVATE ${ZEPHYR_BASE}/subsys/net/ip)

target_sources(app PRIVATE src/main.cpp src/led_light.cpp src/solenoid_doorlock.cpp 
  src/rotating_doorlock.cpp src/led_doorlock.cpp src/led_rot_doorlock.cpp src/doorlock.cpp
  src/keypad_input.cpp src/shell_input.cpp src/input_state_machine.cpp src/coap_input.cpp)
