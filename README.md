<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Doorlock Zephyr Application

[[_TOC_]]

## Overview

A Zephyr application demonstrating a PoC smart door lock, operating several 
types of locks. 

This is part of the Oniro Project Doorlock Blueprint, for further information
follow the 
[Doorlock Blueprint documentation page](https://docs.oniroproject.org/projects/blueprints/doorlock.html)

### Requirements
- Supported boards:
  - Arduino Nano 33 BLE
