.. _doorlock:

Doorlock
########

Overview
********

Doorlock drives a lock solenoid using the :ref:`GPIO
API <gpio_api>`.

.. _doorlock-requirements:

Requirements
************

You will see this error if you try to build doorlock for an unsupported board:

.. code-block:: none

   Unsupported board: doorlock0 devicetree alias is not defined

The board must have a lock solenoid connected via a GPIO pin.
The GPIO pin must be configured using the
``doorlock0`` :ref:`devicetree <dt-guide>` alias. This is usually done in
a :ref:`devicetree overlay <set-devicetree-overlays>`.

Building and Running
********************

Build and flash Doorlock as follows, changing ``arduino_nano_33_ble`` for your board:

.. zephyr-app-commands::
   :zephyr-app: doorlock
   :board: arduino_nano_33_ble
   :goals: build flash
   :compact:

After flashing, the solenoid unlocks and re-locks the lock solenoid attached
to pins D2 and D3 on an Arduino Nano 33 BLE (or equivalent) 5 times.
