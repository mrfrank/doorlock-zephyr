/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifdef CONFIG_COAP_INPUT

#include "coap_input.hpp"

#include <logging/log.h>
LOG_MODULE_REGISTER(coap_server, LOG_LEVEL_DBG);

#include <zephyr/net/coap.h>
#include <zephyr/net/coap_link_format.h>

extern "C" {
	#include <net_private.h>
	#include <ipv6.h>
}

#include <errno.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/zephyr.h>

#include <zephyr/net/socket.h>
#include <zephyr/net/net_mgmt.h>
#include <zephyr/net/net_ip.h>
#include <zephyr/net/udp.h>

coap_input::coap_input(std::string port) {
	int r = start_coap_server(std::stoul(port));
	if (r < 0) {
		LOG_ERR("Could not start coap server");
		throw(errno);
	}
    input_devices::add_input(this);
}

bool coap_input::join_coap_multicast_group(uint16_t port)
{
	static struct in6_addr my_addr;
	static struct sockaddr_in6 mcast_addr = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(port),
		.sin6_addr = ALL_NODES_LOCAL_COAP_MCAST };
	struct net_if_addr *ifaddr;
	struct net_if *iface;
	int ret;

	iface = net_if_get_default();
	if (!iface) {
		LOG_ERR("Could not get the default interface\n");
		return false;
	}

	if (net_addr_pton(AF_INET6,
			  CONFIG_NET_CONFIG_MY_IPV6_ADDR,
			  &my_addr) < 0) {
		LOG_ERR("Invalid IPv6 address %s",
			CONFIG_NET_CONFIG_MY_IPV6_ADDR);
		throw(errno);
	}


	ifaddr = net_if_ipv6_addr_add(iface, &my_addr, NET_ADDR_MANUAL, 0);
	if (!ifaddr) {
		LOG_ERR("Could not add unicast address to interface");
		return false;
	}

	ifaddr->addr_state = NET_ADDR_PREFERRED;

	ret = net_ipv6_mld_join(iface, &mcast_addr.sin6_addr);
	if (ret < 0) {
		LOG_ERR("Cannot join %s IPv6 multicast group (%d)",
			net_sprint_ipv6_addr(&mcast_addr.sin6_addr), ret);
		return false;
	}

	LOG_DBG("joined multicast group");
	return true;
}

int coap_input::start_coap_server(uint16_t port)
{
	join_coap_multicast_group(port);

	struct sockaddr_in6 addr6;
	memset(&addr6, 0, sizeof(addr6));
	addr6.sin6_family = AF_INET6;
	addr6.sin6_port = htons(port);

	this->_m_sockfd = zsock_socket(addr6.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
	if (_m_sockfd < 0) {
		LOG_ERR("Failed to create UDP socket %d", errno);
		return -errno;
	}

	int r = zsock_bind(_m_sockfd, (struct sockaddr *)&addr6, sizeof(addr6));
	if (r < 0) {
		LOG_ERR("Failed to bind UDP socket %d", errno);
		return -errno;
	}

	return 0;
}

int coap_input::fd() const noexcept {
    return this->_m_sockfd;
}

long coap_input::read_event() const {
    int received;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	uint8_t request[MAX_COAP_MSG_LEN];

	client_addr_len = sizeof(client_addr);
    received = zsock_recvfrom(this->_m_sockfd, request, sizeof(request), 0,
				&client_addr, &client_addr_len);

    if (received < 0) {
		LOG_ERR("Connection error %d", errno);
		return -errno;
	}

    struct coap_packet packet;
	struct coap_option options[16] = { 0 };
	uint8_t opt_num = 16U;

    int r = coap_packet_parse(&packet, request, received, NULL, 0);
 	if (r < 0) {
		LOG_ERR("Invalid data received (%d)\n", r);
		return -1;
	}

    uint16_t payload_len = 1;
    const uint8_t *payload = coap_packet_get_payload(&packet, &payload_len);	
    if(payload_len != 1) {
        LOG_ERR("Only single key presses are supported at a time");
        return -1;
    }

    return payload[0];
}


coap_input coap_input_device("5683");

#endif // CONFIG_COAP_INPUT
