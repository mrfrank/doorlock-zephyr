/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 * Luca Seritan <luca.seritan@huawei.com>
 */

#include <drivers/gpio.h>
#include <sys/printk.h>
#include "doorlock.hpp"

doorlock::doorlock() : _m_state(locking_state::UNLOCKED) { }

void doorlock::lock()
{
    if (this->is_locked())
        return;
    this->_m_state = locking_state::LOCKED;
}

void doorlock::unlock()
{
    if (!this->is_locked())
        return;
    this->_m_state = locking_state::UNLOCKED;
}

bool doorlock::is_locked()
{
    return this->_m_state == locking_state::LOCKED;
}
