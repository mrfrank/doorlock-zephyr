/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */

#define NO_ACTION 0
#define ERROR -1
#define CORRECT_PIN 1
#define WRONG_PIN 2
#define CHANGED_PIN 3
#define CHANGING_PIN 4
#define TIMEOUT 5

#include "input_state_machine.hpp"
#include <zephyr/sys/printk.h>

input_state_machine::input_state_machine() {
    this->_m_current_input = "";
    this->_m_current_state = input_state::STATE_INITIAL_PIN;

    using std::placeholders::_1;
    this->_m_state_process[input_state::STATE_RESET] = std::bind(&input_state_machine::state_reset_process, this, _1);
    this->_m_state_process[input_state::STATE_INPUT_PIN] = std::bind(&input_state_machine::state_input_pin_process, this, _1);
    this->_m_state_process[input_state::STATE_CHANGE_PIN] = std::bind(&input_state_machine::state_change_pin_process, this, _1);
    this->_m_state_process[input_state::STATE_INITIAL_PIN] = std::bind(&input_state_machine::state_initial_pin_process, this, _1);
}

int input_state_machine::process(char event) {
    if (event != '*' && event != '#' && !isdigit(event))
        return ERROR;
    return _m_state_process[_m_current_state](event);
}


int input_state_machine::state_reset_process(char event) {
    if (event == '#') {
        this->_m_current_state = input_state::STATE_CHANGE_PIN;
        return CHANGING_PIN;
    }
        
    if (std::isdigit(event)) {
        this->_m_current_input += event;
        this->_m_current_state = input_state::STATE_INPUT_PIN;
    }

    return NO_ACTION;
}

int input_state_machine::state_input_pin_process(char event) {
    if (event == '*') {
        this->_m_current_input = "";
        this->_m_current_state = input_state::STATE_RESET;
    }

    if (std::isdigit(event)) {
        this->_m_current_input += event;
        if (this->_m_current_input.size() != 4) {
            return NO_ACTION;
        }
        if (this->_m_current_input == _m_pin) {
            this->_m_current_input = "";
            return CORRECT_PIN;
        } else {
            this->_m_current_input = "";
            return WRONG_PIN;
        } 
    }

    return NO_ACTION;
}

int input_state_machine::state_change_pin_process(char event) {
    if (event == '*') {
        this->_m_current_input = "";
        this->_m_current_state = input_state::STATE_RESET;
        return NO_ACTION;
    }

    if (std::isdigit(event)) {
        this->_m_current_input += event;
        if (this->_m_current_input.size() != 4) {
            return CHANGING_PIN;
        } else {
            this->_m_pin = _m_current_input;
            _m_current_input = "";
            this->_m_current_state = input_state::STATE_INPUT_PIN;
            return CHANGED_PIN;
        }
    }

    return NO_ACTION;
}

int input_state_machine::state_initial_pin_process(char event) {
    if (std::isdigit(event)) {
        this->_m_current_input += event;
        if (this->_m_current_input.size() != 4) {
            return CHANGING_PIN;
        } else {
            this->_m_pin = this->_m_current_input;
            printk("New pin: %s\n", this->_m_pin.c_str());
            _m_current_input = "";
            this->_m_current_state = input_state::STATE_INPUT_PIN;
            return CHANGED_PIN;
        }
    }

    return NO_ACTION; 
}
