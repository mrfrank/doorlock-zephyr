/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */

#ifndef STATE_MACHINE_HPP
#define STATE_MACHINE_HPP

#define NO_ACTION 0
#define ERROR -1
#define CORRECT_PIN 1
#define WRONG_PIN 2
#define CHANGED_PIN 3
#define CHANGING_PIN 4
#define TIMEOUT 5

#include <string>
#include <functional>
#include <map>

class input_state_machine {
public:  
   
    input_state_machine();

    int process(char event);

private:
    enum class input_state {
        STATE_RESET = 1,
        STATE_INPUT_PIN,
        STATE_CHANGE_PIN,
        STATE_INITIAL_PIN
    };

    std::string _m_pin;
    std::string _m_current_input;
    input_state _m_current_state;
    std::map<input_state, std::function<int(char)> > _m_state_process;

    int state_reset_process(char event);
    int state_input_pin_process(char event);
    int state_change_pin_process(char event);
    int state_initial_pin_process(char event);
};

#endif // STATE_MACHINE_HPP
