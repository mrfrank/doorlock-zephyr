/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifdef CONFIG_KEYPAD_INPUT

#include "input_device.hpp"
#include "keypad_input.hpp"
#include <zephyr/drivers/gpio.h>
#include <zephyr/net/socket.h>
#include <zephyr/posix/unistd.h>
#include <zephyr/zephyr.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/util.h>
#include <zephyr/sys/printk.h>
#include <inttypes.h>
#include <zephyr/kernel.h>
#include <string>

#define MY_STACK_SIZE 512
#define MY_PRIORITY 5

namespace {
    const struct gpio_dt_spec kbd[7] = {
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd0), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd1), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd2), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd3), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd4), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd5), gpios, {0}),
        GPIO_DT_SPEC_GET_OR(DT_ALIAS(kbd6), gpios, {0})
    };

    const int col_pins[3] = { 6, 2, 1 };
    const int row_pins[4] = { 5, 0, 4, 3 };
    const char keys[4][3] = {
        { '1', '2', '3' },
        { '4', '5', '6' },
        { '7', '8', '9' },
        { '*', '0', '#' }
    };

    char pressedKeys[13];

    gpio_callback gpio_cb;

    struct k_work_wrapper {
        k_work work;
        char pressed[13];
        int fd;
    }; 
    
    k_work_wrapper work_wrapper;
}

void gpio_handler(const device* port,
                        gpio_callback *_cb,
                        gpio_port_pins_t pins) {
    keypad_input::handler_wrapper *wrapper = CONTAINER_OF(_cb, keypad_input::handler_wrapper, cb); 
    wrapper->input->process();
}

void write_to_fd(k_work *item) {
    k_work_wrapper *wrapper = CONTAINER_OF(item, k_work_wrapper, work);

    if(strlen(wrapper->pressed) == 0) {
        return;
    } 

    int ret = write(wrapper->fd, wrapper->pressed, strlen(wrapper->pressed));
    if (ret < 0) {
        printk("Error %d while writing to file descriptor %d, return val is %d\n", errno, wrapper->fd, ret);
        throw(errno);
    }
}
   
keypad_input::keypad_input() 
    : _m_wrapper(gpio_cb, this)
{
    for(int i=0; i<7; i++) {
        int ret = gpio_pin_configure_dt(&kbd[i], GPIO_INPUT | GPIO_PULL_UP);
        if(ret != 0) {
            printk("gpio_pin_configure_dt failed with error %d \n", errno);
            throw(errno);
        }
    }

    int ret = zsock_socketpair(AF_UNIX, SOCK_STREAM, 0, this->_m_fd);
    if (ret != 0) {
        printk("Error %d while creating socket pair\n", errno);
    }

    for(int row = 0; row < 4; ++row) {
        gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_OUTPUT);
        gpio_pin_set_dt(&kbd[row_pins[row]], 0);
    }

    for(int i = 0; i < 3; ++i) {
        int ret = gpio_pin_interrupt_configure_dt(&kbd[col_pins[i]], GPIO_INT_EDGE_FALLING);
        if(ret != 0) {
            printk("Interrupt adding failed with error %d\n", errno);
            throw(errno);
        }
    } 

    int mask = 0;
    for(int i = 0; i < 3; ++i) {
        mask |= (1<<kbd[col_pins[i]].pin);
    }

    gpio_init_callback(&_m_wrapper.cb, gpio_handler, mask);

    for(int i = 0; i < 3; ++i) {
        int ret = gpio_add_callback(kbd[col_pins[i]].port, &_m_wrapper.cb);
        if (ret != 0) {
            printk("Error %d adding callback\n", -errno);
        }
    }

    input_devices::add_input(this);
}

long keypad_input::read_event() const {
    long key;
    int ret = read(this->_m_fd[1], &key, 1);
    if(ret < 0) {
        printk("Error while reading from file descriptor");
        throw(errno);
    }
    
    printk("Received event %c\n", (char)key);
    return key;
}

int keypad_input::fd() const noexcept {
    return this->_m_fd[1];
}

void keypad_input::process() {
    memset(pressedKeys, 0, 13);

	for(int i=0; i<4; i++)
		gpio_pin_configure_dt(&kbd[row_pins[i]], GPIO_INPUT | GPIO_PULL_UP);

    for(int i = 0; i < 3; ++i) {
        int ret = gpio_pin_interrupt_configure_dt(&kbd[col_pins[i]], GPIO_INT_DISABLE);
        if(ret != 0) {
            printk("Interrupt adding failed with error %d\n", errno);
            throw(errno);
        }
    }

	for(int row=0; row<4; row++) {
		gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_OUTPUT);
		gpio_pin_set_dt(&kbd[row_pins[row]], 0);
		for(int col=0; col<3; col++) {
			if(!gpio_pin_get_dt(&kbd[col_pins[col]]))
			    pressedKeys[strlen(pressedKeys)] = keys[row][col];
		}
		gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_INPUT | GPIO_PULL_UP);
	}

    for(int row = 0; row < 4; ++row) {
        gpio_pin_configure_dt(&kbd[row_pins[row]], GPIO_OUTPUT);
        gpio_pin_set_dt(&kbd[row_pins[row]], 0);
    }    
    
    for(int i = 0; i < 3; ++i) {
        int ret = gpio_pin_interrupt_configure_dt(&kbd[col_pins[i]], GPIO_INT_EDGE_FALLING);
        if(ret != 0) {
            printk("Interrupt adding failed with error %d\n", errno);
            throw(errno);
        }
    } 

    work_wrapper.fd = _m_fd[0]; 
    k_work_init(&work_wrapper.work, write_to_fd);
    memset(work_wrapper.pressed, 0, sizeof(work_wrapper.pressed));
    strcpy(work_wrapper.pressed, pressedKeys);
    
    k_work_submit(&work_wrapper.work);
}

keypad_input keypad_input;

#endif // CONFIG_KEYPAD_INPUT
