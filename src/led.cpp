/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#include "led.hpp"
#include "compat.h"
#include <drivers/gpio.h>
#include <string.h>
#include <sys/printk.h>
#include <zephyr.h>

#define LED0_NODE DT_ALIAS(led0)
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
static struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET_OR(LED0_NODE, gpios, {0});
#else
#warning led0 not defined in devicetree, LED commands will be ignored
static gpio_dt_spec led0 = {NULL, 0, 0};
#endif

int __led_status = 0x00;

K_FIFO_DEFINE(led_blink_thread_control);
struct led_blink_thread_msg_t
{
  void *fifo_reserved;
  int frequency;
};

namespace {

void led_blink_thread_handler(void *p1, void *p2, void *p3)
{
    while (1) {
      struct led_blink_thread_msg_t *data =
          (struct led_blink_thread_msg_t *)k_fifo_get(&led_blink_thread_control,
                                                      K_FOREVER);
      int frequency = data->frequency;
      k_free(data);
      while (frequency) {
        led_toggle();
        data = (led_blink_thread_msg_t *)k_fifo_get(&led_blink_thread_control,
                                                    K_MSEC(frequency));
        if (data)
        {
          frequency = data->frequency;
          k_free(data);
        }
      }
      led_off();
   }
}

} /* namespace */


K_THREAD_DEFINE(led_blink_thread, 512, led_blink_thread_handler, NULL, NULL,
                NULL, 7, 0, 0);

void led_init()
{
    if (led0.port && !device_is_ready(led0.port)) {
      printk("LED led0 not ready, LED commands will be ignored\n");
      led0.port = NULL;
    }
    if (led0.port) {
      gpio_pin_configure_dt(&led0, GPIO_OUTPUT);
      gpio_pin_set_dt(&led0, 1);
    }
    else {
      throw(ENODEV);
    }
}

void led_on()
{
    gpio_pin_set_dt(&led0, 0);
    __led_status |= 1;
}

void led_off()
{
    gpio_pin_set_dt(&led0, 1);
    __led_status &= ~1;
}

void led_toggle()
{
    gpio_pin_set_dt(&led0, __led_status & 1);
    __led_status ^= 1;
}

void led_blink()
{
    printk("%d ", __led_status);
    led_toggle();
    k_msleep(500);
    led_toggle();
    k_msleep(500);
}

void led_start_blinking(int frequency)
{
    if (!frequency && !(__led_status & int(led_status::BLINKING)))
      return;
    
    struct led_blink_thread_msg_t msg = {.frequency = frequency};
    const size_t size = sizeof(struct led_blink_thread_msg_t);
    char *mem = (char *)k_malloc(size);
    memcpy(mem, &msg, size);
    k_fifo_put(&led_blink_thread_control, mem);
    if (frequency)
      __led_status |= int(led_status::BLINKING);
    else
      __led_status &= ~int(led_status::BLINKING);
}

void led_stop_blinking()
{
    led_start_blinking(0);
}

int led_status()
{
    return __led_status;
}
