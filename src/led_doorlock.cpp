/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_LED_DOORLOCK

#include "led_doorlock.hpp"

/**
 * @brief Lock Led implementation
 * It's not even a real doorlock, its sole purpose
 * is to test the class implementation introduced in the new version
 */
led_doorlock::led_doorlock() : doorlock(),
                               _led_light()
{ 

}

void led_doorlock::unlock() 
{
    this->doorlock::unlock();

    this->_led_light.led_off(); 
}

void led_doorlock::lock()
{ 
    this->doorlock::lock();

    this->_led_light.led_on();
}

#endif
