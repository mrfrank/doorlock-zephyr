/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_LED_DOORLOCK

#ifndef LED_DOORLOCK_HPP
#define LED_DOORLOCK_HPP

#include "doorlock.hpp"
#include "led_light.hpp"

class led_doorlock : public doorlock
{
public:
    led_doorlock();
    virtual void unlock();
    virtual void lock();
private:
    /**
     * @brief Light object
     * Object used for led light
     */
    led_light _led_light;
    const device *Pin0, *Pin1;
};

#endif //LED_DOORLOCK_HPP   
#endif //CONFIG_LED_DOORLOCK
