/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifndef LED_LIGHT_HPP
#define LED_LIGHT_HPP

#include <zephyr.h>
#include <drivers/gpio.h>

enum class led_status
{
    OFF = 0x00,
    ON = 0x01,
    BLINKING = 0x02
};

class led_light
{
private:
    k_thread led_thread_data;
    k_tid_t led_tid;
	
    /**
     * @brief Status of the LED
     */
    int __led_status;

public:
	
    /**
    * @brief Initialize the LED
    */
    led_light();

    /**
    * @brief Turn the LED on
    */
    void led_on();

    /**
    * @brief Turn the LED off
    */
    void led_off();

    /**
    * @brief Toggle the LED
    */
    void led_toggle();

    /**
     * @brief Blink the LED once (toggle for 1 second, then toggle back)
     */
    void led_blink();

    /**
     * @brief Start blinking the LED
     * @param frequency frequency in milliseconds
     */
    void led_start_blinking(int frequency);

    /**
     * @brief Stop blinking the LED
     */
    void led_stop_blinking();

};

#endif
