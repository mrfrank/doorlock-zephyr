/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_LED_ROT_DOORLOCK

#include "led_rot_doorlock.hpp"
#include "doorlock.hpp"
#include "led_light.hpp"
#include <zephyr/logging/log.h>

#define DOORLOCK0_NODE DT_ALIAS(doorlock0)
#if DT_NODE_HAS_STATUS(DOORLOCK0_NODE, okay)
#define DOORLOCK0 DT_GPIO_LABEL(DOORLOCK0_NODE, gpios)
#define PIN0 DT_GPIO_PIN(DOORLOCK0_NODE, gpios)
#define FLAGS0 DT_GPIO_FLAGS(DOORLOCK0_NODE, gpios)
#else
#error "Unsupported board: doorlock0 devicetree alias is not defined"
#endif

#define DOORLOCK1_NODE DT_ALIAS(doorlock1)
#if DT_NODE_HAS_STATUS(DOORLOCK1_NODE, okay)
#define DOORLOCK1 DT_GPIO_LABEL(DOORLOCK1_NODE, gpios)
#define PIN1 DT_GPIO_PIN(DOORLOCK1_NODE, gpios)
#define FLAGS1 DT_GPIO_FLAGS(DOORLOCK1_NODE, gpios)
#else
#error "Unsupported board: doorlock1 devicetree alias is not defined"
#endif

#define CONFIG_LED_LOG_LEVEL 4
LOG_MODULE_DECLARE(led_rotating_doorlock, CONFIG_LED_LOG_LEVEL);

/**
 * @brief Lock Rotating Led implementation
 * It's not even a real doorlock, its sole purpose
 * is to test the class implementation introduced in the new version
 * This also tests CONTAINER_OF functionalities implemented
 * in RotatingDoorlock class
 * 
 * Right now, it prints in the console besides led lighting
 * 
 */

void lock_stop_led_rotating_timer_handler(k_timer *timer_id)
{
    LOG_DBG("Got to this timer_handler...\n");
    led_rotating_doorlock::timer_wrapper *wrapper =
        CONTAINER_OF(timer_id, led_rotating_doorlock::timer_wrapper, timer);
    wrapper->lock->lock_rotating_stop();
}

led_rotating_doorlock::led_rotating_doorlock() : doorlock(),
                                               _led_light(), 
                                               wrapper(this)                                                   
{
    LOG_INF("Initialized\n");
	
    k_timer_init(&wrapper.timer, lock_stop_led_rotating_timer_handler, NULL);
}

void led_rotating_doorlock::lock_rotating_rotate_clockwise()
{
    this->_led_light.led_on();
    LOG_INF("Locking...\n");
}

void led_rotating_doorlock::lock_rotating_rotate_counterclockwise()
{
    this->_led_light.led_off();
    LOG_INF("Unlocking...\n");
}

void led_rotating_doorlock::lock_rotating_stop()
{
    LOG_DBG("Got to lock_rotating_stop!\n");
    // No point in leaving an auto-stop running if we've already stopped it
    k_timer_stop(&wrapper.timer);
}

void led_rotating_doorlock::unlock()
{
    this->doorlock::unlock();

    this->lock_rotating_rotate_counterclockwise();
    k_timer_start(&wrapper.timer, K_SECONDS(1), K_NO_WAIT);
}

void led_rotating_doorlock::lock()
{
    this->doorlock::lock();

    this->lock_rotating_rotate_clockwise();
    // It takes roughly 13 seconds to go from fully open to fully closed
    // but for the testing purpose we are waiting only 1 second
    k_timer_start(&wrapper.timer, K_SECONDS(1), K_NO_WAIT);
}

void led_rotating_doorlock::unlock_lock(int time)
{
    this->lock();
    //we have to wait for at least a second
    if (time < 1)
        time = 1;
    k_timer_start(&wrapper.timer, K_SECONDS(time), K_NO_WAIT);
    this->unlock();
}

#endif
