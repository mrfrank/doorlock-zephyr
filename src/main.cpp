/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#include <zephyr.h>
#include <sys/printk.h>
#include <string>
#include <vector>
#include <posix/posix_types.h>
#include <posix/unistd.h>
#include <posix/sys/select.h>
#include "input_state_machine.hpp"
#include "input_device.hpp"
#include "doorlock.hpp"
#include "led_doorlock.hpp"
#include "led_rot_doorlock.hpp"
#include "rotating_doorlock.hpp"
#include "solenoid_doorlock.hpp"
#include <zephyr/usb/usb_device.h>

#ifdef CONFIG_LED_DOORLOCK
doorlock *lock = new led_doorlock();
#elif CONFIG_LED_ROT_DOORLOCK
doorlock *lock = new led_rot_doorlock();
#elif CONFIG_ROTATING_DOORLOCK
doorlock *lock = new rotating_doorlock();
#elif CONFIG_SOLENOID_DOORLOCK
doorlock *lock = new solenoid_doorlock();
#else
doorlock *lock = NULL
#endif

namespace {
    std::vector<input_device*> devices;
};

void input_devices::add_input(input_device *device) {
    devices.push_back(device);
}

int main(int argc, char **argv) {
    int fd_max = 0;  
    fd_set read_fds;
    fd_set tmp_fds;
    FD_ZERO(&read_fds);

    if (usb_enable(NULL)) {
        return -1;
    }
  
    for(int i = 0; i < (int)devices.size(); ++i) {
        FD_SET(devices[i]->fd(), &read_fds);
        fd_max = std::max(fd_max, devices[i]->fd());
    } 
  
    input_state_machine state_machine;
    int last_out = CHANGING_PIN;

    if (lock == NULL) {
        printk("Please enable a doorlock device in prj.conf\n");
        return -1;
    }

    printk("Insert initial pin\n");
    while(1) {
        tmp_fds = read_fds;
        zsock_timeval timeout;
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        int ret = select(fd_max + 1, &tmp_fds, NULL, NULL, &timeout);
        if (ret < 0) {
            printk("Error while polling file descriptors\n");
            throw(errno);
            return ret;
        }

        for (int i = 0; i < (int)devices.size(); ++i) { 
            if(FD_ISSET(devices[i]->fd(), &tmp_fds)) {
                int ret = state_machine.process((char)devices[i]->read_event());
                if (ret == ERROR) {
                    printk("Error in state machine, unrecognized key, ignoring...\n");
                }

                if (last_out != CHANGING_PIN && ret == CHANGING_PIN) {
                    printk("Insert the new pin:\n");
                }     
                if (ret == CHANGED_PIN) {
                    printk("Pin changed successfully\n");
                }
                if (ret == CORRECT_PIN) {
                    if(lock->is_locked()) {
                        printk("Correct pin, unlocking doorlock\n");
                        lock->unlock();
                    } else {
                        printk("Correct pin, locking doorlock\n");
                        lock->lock();
                    }
                }
                if (ret == WRONG_PIN) {
                    printk("Wrong pin, try again\n");
                }

                last_out = ret;
            }
        }
    }

    return 0;
}
