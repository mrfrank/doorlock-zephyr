/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_ROTATING_DOORLOCK

#ifndef ROTATING_DOORLOCK_HPP
#define ROTATING_DOORLOCK_HPP

#include "doorlock.hpp"

class rotating_doorlock : public doorlock
{
public:
    rotating_doorlock();
    virtual void unlock();
    virtual void lock();
    /**
     * @brief Unlock, then lock the lock
     * It is usually a good idea to use unlock_lock
     * rather than unlock because keeping the doorlock in
     * its unlocked state may drive up power consumption (the lock
     * is essentially pulled in by a strong electric magnet).
     * @param time Time in seconds to keep unlocked
     */
    virtual void unlock_lock(int time);

private:

    /**
     * @brief Wrapper for doorlock
     * In-built zephyr function uses timer as a parameter,
     * however there is a need for interacting with
     * doorlock, hence we need this kind of wrapper
     */
    struct timer_wrapper
    {
        timer_wrapper(rotating_doorlock * new_lock) :
                      lock(new_lock) { };

        rotating_doorlock *lock;
        k_timer timer;
    };
    timer_wrapper wrapper;
    /**
     *  @brief Stop the motor
     */
    void lock_rotating_stop();

    /**
     * @brief Start rotating clockwise (locking)
     */
    void lock_rotating_rotate_clockwise();

    /**
     * @brief Start rotating counterclockwise (unlocking)
     */
    void lock_rotating_rotate_counterclockwise();
    const device *Pin0, *Pin1;

    /**
     * @brief Handler for timer
     */    
    friend void lock_stop_rotating_timer_handler(k_timer *timer_id);
};

#endif //ROTATING_DOORLOCK_HPP
#endif //CONFIG_ROTATING_DOORLOCK
