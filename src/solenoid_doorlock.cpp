/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_SOLENOID_DOORLOCK

#include "solenoid_doorlock.hpp"
#include "doorlock.hpp"

#define DOORLOCK0_NODE DT_ALIAS(doorlock0)
#if DT_NODE_HAS_STATUS(DOORLOCK0_NODE, okay)
#define DOORLOCK0 DT_GPIO_LABEL(DOORLOCK0_NODE, gpios)
#define PIN0 DT_GPIO_PIN(DOORLOCK0_NODE, gpios)
#define FLAGS0 DT_GPIO_FLAGS(DOORLOCK0_NODE, gpios)
#else
#error "Unsupported board: doorlock0 devicetree alias is not defined"
#endif

#define DOORLOCK1_NODE DT_ALIAS(doorlock1)
#if DT_NODE_HAS_STATUS(DOORLOCK1_NODE, okay)
#define DOORLOCK1 DT_GPIO_LABEL(DOORLOCK1_NODE, gpios)
#define PIN1 DT_GPIO_PIN(DOORLOCK1_NODE, gpios)
#define FLAGS1 DT_GPIO_FLAGS(DOORLOCK1_NODE, gpios)
#else
#error "Unsupported board: doorlock1 devicetree alias is not defined"
#endif

/**
 * @brief Lock Solenoid implementation
 */
solenoid_doorlock::solenoid_doorlock() : doorlock(), 
                                         Pin0(device_get_binding(DOORLOCK0)),
                                         Pin1(device_get_binding(DOORLOCK1))                                       
{
    _m_state = locking_state::UNLOCKED;
    if (!this->Pin0) {
        printk("Can't get devicetree entry for doorlock0\n");
        throw(ENODEV);
    }

    if (!this->Pin1) {
        printk("Can't get devicetree entry for doorlock0\n");
        throw(ENODEV);
    }

    if (gpio_pin_configure(this->Pin0, PIN0, GPIO_OUTPUT_ACTIVE | FLAGS0) < 0) {
        printk("Can't configure solenoid pin 0\n");
        throw(ENODEV);
    }

    if (gpio_pin_configure(this->Pin1, PIN1, GPIO_OUTPUT_ACTIVE | FLAGS1) < 0) {
        printk("Can't configure solenoid pin 1\n");
        throw(ENODEV);
    }
}

void solenoid_doorlock::unlock()
{
    this->doorlock::unlock();

    gpio_pin_configure(this->Pin0, PIN0, FLAGS0 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_LOW);
    gpio_pin_configure(this->Pin1, PIN1,
                         FLAGS1 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_HIGH);
}

void solenoid_doorlock::lock()
{
    this->doorlock::lock();   

    gpio_pin_configure(this->Pin0, PIN0, FLAGS0);
    gpio_pin_configure(this->Pin1, PIN1, FLAGS1);
}

#endif
